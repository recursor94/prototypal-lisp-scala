import scala.util.parsing.combinator._

abstract class LispExpression
class LispObject(id: String, typeSlot: String= "Object") extends LispExpression
case class Cons(id: String, typeSlot: String ="Cons Cell", car: LispObject, cdr: Cons) extends LispObject(id, typeSlot) //Every cons cell is made of a car containing first element; cdr containing the next cons cell
case class LispList(id: String, typeSlot: String = "List", firstCell: Cons) extends LispObject(id,typeSlot)
case class LispSymbol(id: String, typeSlot: String = "Symbol", symbol: Symbol) extends LispObject(id,typeSlot)
case class LispNumber(id: String, typeSlot: String = "Number", value: Number) extends LispObject(id,typeSlot)
case class LispString(id: String, typeSlot: String = "String", value: String) extends LispObject(id, typeSlot)
case class SuppressedExpression(expression: LispExpression) extends LispExpression //To represent quoted expressions which will not be evaluated

class InputParser extends JavaTokenParsers {
  def exp: Parser[LispExpression] = "(" ~ exp ~ ")" ^^ {case openParen ~ expr ~ closingParen => LispList("","List", cons(exp))}
  def cons: Parser[LispExpression] = 
  def symb: Parser[LispExpression] = "[A-Za-z].r" ^^ {case symb => LispSymbol("","Symbol", Symbol(symb))}
  def num: Parser[LispExpression] = "[0-9].r" ^^ {case number => LispNumber("","Number", number.toInt)}
  def str: Parser[LispExpression] = ""
  def quot: Parser[LispExpression]= "`" ~ exp ^^ {case backtick ~ expr => SuppressedExpression(expr)}
}


object REPL extends App {
  println("Lisp> ")
}
